var express = require('express');
var app = express();
const puppeteer = require('puppeteer');


app.get('/send', function(req, res) {

    typeMessage(message = 'OI msg pela api');
    async function typeMessage(message) {
        let parts = message.split('\n');
        for (var i = 0; i < parts.length; i++) {
            await page.keyboard.down('Shift');
            await page.keyboard.press('Enter');
            await page.keyboard.up('Shift');
            await page.keyboard.type(parts[i]);
        }
        await page.keyboard.press('Enter');

        // verify message is sent
        let messageSent = await page.evaluate((selector) => {
            let nodes = document.querySelectorAll(selector);
            let el = nodes[nodes.length - 1];
            return el ? el.innerText : '';
        }, selector.last_message_sent);

        if (message == messageSent) {
            print("You: " + message, config.sent_message_color);

            // setup interval for read receipts
            if (config.read_receipts) {
                last_sent_message_interval = setInterval(function() {
                    isLastMessageRead(user, message);
                }, (config.check_message_interval));
            }

        }
    }
    res.send('Hello World!');
});

app.listen(3001);