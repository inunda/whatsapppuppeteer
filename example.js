const { Client } = require('./index')
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(3001);

var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'techmove.dataenv.com.br',
    port: 33306,
    user: 'admin',
    password: 'policy=99',
    database: 'linkl_homologacao'
});

connection.connect();


const client = new Client({ puppeteer: { headless: true } });
// You can use an existing session and avoid scanning a QR code by adding a "session" object to the client options.
// This object must include WABrowserId, WASecretBundle, WAToken1 and WAToken2.

app.get('/qrcode', function(req, res) {
    client.initialize();
    res.send('QR CODE GERADO');
});
app.post('/send', function(req, res) {
    let numero = req.body.numero;
    let msg = req.body.msg;
    client.sendMSG(numero, msg);

    res.send(msg);
});

client.on('qr', (qr) => {
    // NOTE: This event will not be fired if a session is specified.
    console.log('QR RECEBIDO', qr);
});

client.on('authenticated', (session) => {
    console.log('AUTENTICADO', session);
});

client.on('auth_failure', msg => {
    // Fired if session restore was unsuccessfull
    console.error('FALA NA AUTENTICACAO', msg);
})

client.on('ready', () => {
    console.log('PRONTO');
});

client.on('message', async msg => {
    console.log('Mensagem Recebida', msg);
    var telTo = msg.to;
    var telTo2 = telTo.replace('@c.us', '');
    var telFrom = msg.from;
    var telFrom2 = telFrom.replace('@c.us', '');
    connection.query(
        "INSERT INTO `clientes_mensagens_whatsapp` (`empresa_id`, `midia`, `nome`, `mensagem`, `fone`, `origem`, `destino`, `data_cadastro`, `sentido`, `automatico`, `status`, `deletado`) VALUES ('" + telTo2 + "', '" + msg.type + "', '" + telTo2 + "', '" + msg.body + "', '" + telFrom2 + "', '" + telTo2 + "', '" + telFrom2 + "', NULL, 'i', '', '1', '0')",
        function(error, results, fields) {
            if (error) throw error;
        });
    //console.log(query.sql);


    if (msg.body == '!ping reply') {
        // Envia uma nova mensagem como resposta à atual
        msg.reply('pong');

    } else if (msg.body == '!ping') {
        // Send a new message to the same chat
        client.sendMessage(msg.from, 'pong');

    } else if (msg.body.startsWith('!subject ')) {
        // Change the group subject
        let chat = await msg.getChat();
        if (chat.isGroup) {
            let newSubject = msg.body.slice(9);
            chat.setSubject(newSubject);
        } else {
            msg.reply('Este comando pode ser usado apenas em um grupo!');
        }
    } else if (msg.body.startsWith('!echo ')) {
        // Replies with the same message
        msg.reply(msg.body.slice(6));
    } else if (msg.body.startsWith('!desc ')) {
        // Change the group description
        let chat = await msg.getChat();
        if (chat.isGroup) {
            let newDescription = msg.body.slice(6);
            chat.setDescription(newDescription);
        } else {
            msg.reply('Este comando pode ser usado apenas em um grupo!');
        }
    } else if (msg.body == '!leave') {
        // Leave the group
        let chat = await msg.getChat();
        if (chat.isGroup) {
            chat.leave();
        } else {
            msg.reply('Este comando pode ser usado apenas em um grupo!');
        }
    } else if (msg.body == '!groupinfo') {
        let chat = await msg.getChat();
        if (chat.isGroup) {
            msg.reply(`
                *Group Details*
                Name: ${chat.name}
                Description: ${chat.description}
                Created At: ${chat.createdAt.toString()}
                Created By: ${chat.owner.user}
                Participant count: ${chat.participants.length}
            `);
        } else {
            msg.reply('Este comando pode ser usado apenas em um grupo!');
        }
    } else if (msg.body == '!chats') {
        const chats = await client.getChats();
        client.sendMessage(msg.from, `The bot has ${chats.length} chats open.`);
    }
});

client.on('message_create', (msg) => {
    // Fired on all message creations, including your own
    console.log('enviada ', msg);
    if (msg.fromMe) {
        var telTo = msg.to;
        var telTo2 = telTo.replace('@c.us', '');
        var telFrom = msg.from;
        var telFrom2 = telFrom.replace('@c.us', '');
        connection.query(
            "INSERT INTO `clientes_mensagens_whatsapp` (`empresa_id`, `midia`, `nome`, `mensagem`, `fone`, `origem`, `destino`, `data_cadastro`, `sentido`, `automatico`, `status`, `deletado`) VALUES ('" + telFrom2 + "', '" + msg.type + "', '" + telFrom2 + "', '" + msg.body + "', '" + telTo2 + "', '" + telTo2 + "', '" + telFrom2 + "', NULL, 'o', '', '2', '0')",
            function(error, results, fields) {
                if (error) throw error;
            });

        //console.log(msg.fromMe);
    }
})

client.on('disconnected', () => {
    console.log('Cliente desconectado');
})